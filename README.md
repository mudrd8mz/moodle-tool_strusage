Strings usage tracking for Moodle
=================================

This is a Moodle plugin allowing to track the actual strings usage on a Moodle site.

Every `get_string()` call gets recorded into a row in a CSV file. The files are stored in a `tool_strusage` folder inside your
dataroot, grouped into directories by `year/month/day/hour/minute/`.

A row in the CSV can look like

    1453814976.5;3;/5/7/8/;5;en_us;forum;discussions

where the values mean (in order):

* `1453814976.5` is a timestamp of the page request (float)
* `3` is the user id
* `/5/7/8/` is the list of all roles that the user has assigned somewhere on the site (may be empty)
* `5` is estimated "major" role that the user can be considered as (such as student vs teacher)
* `en_us` is the current language code
* `forum` is the string component
* `discussions` is the string identifier

Installation
------------

1. Install the plugin
2. Add the following into your config.php file:

    $CFG->customstringmanager = 'tool_strusage\stringman';

3. Check for files created in `tool_strusage` folder inside your dataroot.

Notes
-----

See how many files are created in the folders. Every file represents one page request so you implicitly see how many requests per
minute there was:

    $ cd /path/to/your/moodledata
    $ cd tool_strusage/csv/
    $ find . -mindepth 5 -type d -print0 | while read -d '' -r dir; do files=("$dir"/*); printf "%5d files in directory %s\n" "${#files[@]}" "$dir"; done | sort -n

Monitor the disk usage:

    $ du -hs /path/to/your/moodledata/tool_strusage/csv/
