<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Provides {@link tool_strusage\stringman} class.
 *
 * @package     tool_strusage
 * @copyright   2016 David Mudrak <david@moodle.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace tool_strusage;

use core_string_manager_standard;
use context_course;

defined('MOODLE_INTERNAL') || die();

/**
 * Custom string manager tracking get_string() calls
 *
 * @copyright 2016 David Mudrak <david@moodle.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class stringman extends core_string_manager_standard {

    /** @var storage */
    protected $storage = null;

    /**
     * Creates new instance of the custom string manager.
     *
     * This is called by {@link get_string_manager()} when the singleton is
     * being instantiated.
     *
     * @param string $otherroot location of downloaded lang packs - usually $CFG->dataroot/lang
     * @param string $localroot usually the same as $otherroot
     * @param array $translist limit list of visible translations
     */
    public function __construct($otherroot, $localroot, $translist) {
        parent::__construct($otherroot, $localroot, $translist);
        $this->storage = storage::instance($this);
    }

    /**
     * Track the get_string() call and delegate it to the parent.
     *
     * @param string $identifier the string identifier
     * @param string $component the component providing the string
     * @param string|object|array $a string placeholder data
     * @param string $lang moodle translation language (null for the current one)
     * @return string
     */
    public function get_string($identifier, $component = '', $a = null, $lang = null) {
        static $langconfigstrs = [
            'strftimedate' => 1,
            'strftimedatefullshort' => 1,
            'strftimedateshort' => 1,
            'strftimedatetime' => 1,
            'strftimedatetimeshort' => 1,
            'strftimedaydate' => 1,
            'strftimedaydatetime' => 1,
            'strftimedayshort' => 1,
            'strftimedaytime' => 1,
            'strftimemonthyear' => 1,
            'strftimerecent' => 1,
            'strftimerecentfull' => 1,
            'strftimetime' => 1,
        ];

        $loggedcomponent = $component;

        if (empty($loggedcomponent) and isset($langconfigstrs[$identifier])) {
            $loggedcomponent = 'langconfig';
        }

        if (empty($loggedcomponent) or $loggedcomponent === 'moodle') {
            $loggedcomponent = 'core';
        }

        $loggedlang = $lang;

        if ($loggedlang === null) {
            $loggedlang = current_language();
        }

        $this->track_string($identifier, $loggedcomponent, $loggedlang);

        return parent::get_string($identifier, $component, $a, $lang);
    }

    /**
     * Performs the actual get_string() calls tracking.
     *
     * Subclasses are supposed to implement this - typically to store the
     * tracking data.
     *
     * @param string $identifier the string identifier
     * @param string $component the component providing the string
     * @param string $lang moodle translation language
     */
    protected function track_string($identifier, $component, $lang) {
        global $USER;

        $data = [
            'utime' => $_SERVER['REQUEST_TIME_FLOAT'],
            'userid' => $USER->id,
            'assignedroles' => $this->user_assigned_roles(),
            'currentrole' => (int)$this->user_current_role(),
            'lang' => $lang,
            'component' => $component,
            'stringid' => $identifier,
        ];

        $this->storage->log($data);
    }

    /**
     * Returns a list of all role ids the user has assigned somewhere.
     *
     * Integer IDs are separated and wrapped by a slash to allow easy
     * grepping/searching for the values. Null can be returned for anonymous
     * visitors or admins with no role assigned. If present, values are sorted.
     *
     * @param array $ra explict role assignments (useful mostly for testing only)
     * @return string|null like /5/7/8/
     */
    protected function user_assigned_roles(array $ra = null) {
        global $USER;

        if ($ra === null and !empty($USER->access['ra'])) {
            $ra = $USER->access['ra'];
        }

        if (empty($ra)) {
            return null;
        }

        $roles = array();

        foreach ($ra as $contextpath => $roleassignments) {
            $roles += $roleassignments;
        }

        ksort($roles);

        return '/'.implode('/', array_keys($roles)).'/';
    }

    /**
     * Tries to guess the most appropriate archetype the user should be considered as.
     *
     * This is basically trying to estimate if the current user is viewing the
     * page as a teacher, as a student, as a guest etc. This is not possible to
     * easily decide in all cases, but we try at least something to get more
     * valuable data about strings usage.
     *
     * @param array $ra explict role assignments (useful mostly for testing only)
     * @param string $ctxpath current course's context path (useful mostly for testing only)
     * @return int|null role id
     */
    protected function user_current_role(array $ra = null, $ctxpath = null) {
        global $COURSE, $USER;

        if ($ra === null and !empty($USER->access['ra'])) {
            $ra = $USER->access['ra'];
        }

        if ($ctxpath === null and !empty($COURSE->id) and $COURSE->id != SITEID) {
            $ctxpath = context_course::instance($COURSE->id)->path;
        }

        if (empty($ra) or empty($ctxpath)) {
            return null;
        }

        foreach ($ra as $contextpath => $roleassignments) {
            if ($contextpath === $ctxpath) {
                if (empty($roleassignments)) {
                    // This should not happen but just in case.
                    return null;
                } else {
                    // There should be just one element.
                    return reset($roleassignments);
                }
            }
        }

        return null;
    }
}
