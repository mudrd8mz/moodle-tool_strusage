<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Provides {@link tool_strusage\storage_csv} class.
 *
 * @package     tool_strusage
 * @copyright   2016 David Mudrak <david@moodle.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace tool_strusage;

defined('MOODLE_INTERNAL') || die();

/**
 * Stores string usage data into CSV files in the dataroot.
 *
 * @copyright 2016 David Mudrak <david@moodle.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class storage_csv extends storage {

    /** @var resource */
    protected $fp = null;

    /**
     * Prepare the file to store into.
     */
    protected function init() {
        global $CFG;

        $now = time();
        $root = $CFG->dataroot.'/tool_strusage/csv';
        $path = $root.'/'.date('Y', $now).'/'.date('m', $now).'/'.date('d', $now).'/'.date('H', $now).'/'.date('i', $now);
        make_writable_directory($path);
        $filename = generate_uuid().'.csv';
        $this->fp = fopen($path.'/'.$filename, 'a');
    }

    /**
     * Close the opened file handle.
     */
    protected function done() {
        fclose($this->fp);
    }

    /**
     * Log the string usage data.
     *
     * @param array $data
     */
    public function log(array $data) {
        fputcsv($this->fp, $data, ';');
    }
}
