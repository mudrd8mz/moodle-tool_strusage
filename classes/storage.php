<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Provides {@link tool_strusage\storage} class.
 *
 * @package     tool_strusage
 * @copyright   2016 David Mudrak <david@moodle.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace tool_strusage;

defined('MOODLE_INTERNAL') || die();

/**
 * Base class for storage mechanism classes.
 *
 * Also a factory class for obtaining the particular instance of the storage
 * class.
 *
 * @copyright 2016 David Mudrak <david@moodle.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
abstract class storage {

    /** @var stringman */
    protected $stringman = null;

    /**
     * Factory method returning an instance of the storage mechanism.
     *
     * Called by the custom string manager that will use the storage. At the
     * moment, we have a single subclass implementing the storage. This may be
     * extended in the future.
     *
     * @param stringman $stringman
     * @return storage subclass
     */
    public static function instance(stringman $stringman) {
        return new storage_csv($stringman);
    }

    /**
     * Log the get_string() call usage.
     *
     * @todo We will probably need more formalised format of data (e.g. a model class)
     * @param array $data
     */
    abstract public function log(array $data);

    /**
     * @param stringman $stringman
     */
    protected function __construct(stringman $stringman) {
        $this->stringman = $stringman;
        $this->init();
    }

    /**
     * Destructor allowing to perform something at very end of the life cycle.
     */
    final public function __destruct() {
        $this->done();
    }

    /**
     * Initialize the storage.
     *
     * Called from the constructor. Subclasses may want to implement something
     * useful here.
     */
    protected function init() {
    }

    /**
     * Last chance to do something.
     *
     * Called from the destructor. Subclasses may want to implement something
     * useful here.
     */
    protected function done() {
    }
}
